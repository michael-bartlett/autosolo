import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import { createMuiTheme } from '@material-ui/core/styles';
import AutosoloHeader from './components/AutosoloHeader/AutosoloHeader';
import AutosoloHomeContainer from './home/AutosoloHomeContainer';
import AutosoloDriversContainer from './drivers/AutosoloDriversContainer';
import AutosoloEventContainer from './event/AutosoloEventContainer';
import AutosoloCreateContainer from './create/AutosoloCreateContainer';
import AutosoloGraphContainer from './graph/AutosoloGraphContainer';
import AutosoloTimekeeperContainer from './timekeeper/AutosoloTimekeeperContainer';
import DriversService from './services/DriversService';

import './App.css';

const theme = createMuiTheme({
  overrides: {
    MuiButton: {
      root: {
        margin: '12px',
        'min-width': '7em',
      },
    },
  },
});


const calculateRunTotal = (toCount, results) => {
  const { total, ...runResults } = results;
  if (Object.keys(runResults).length >= toCount) {
    return Object.values(runResults)
      .sort((a, b) => a - b)
      .slice(0, toCount)
      .reduce((a, b) => (a * 100 + b * 100) / 100);
  }
  return undefined;
};

const calculateTotal = (courses, results) => {
  const totals = Object.values(results)
    .filter(course => course && course.total);

  if (totals.length === courses) {
    return totals.map(course => (course && course.total))
      .reduce((a, b) => (a * 100 + b * 100) / 100);
  }
  return undefined;
};

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      event: {
      },
      drivers: [
      ],
    };
    this.setEvent = this.setEvent.bind(this);
    this.setDrivers = this.setDrivers.bind(this);
    this.setConfig = this.setConfig.bind(this);
    this.setBasicRuns = this.setBasicRuns.bind(this);

    this.addDriver = this.addDriver.bind(this);
    this.updateDriver = this.updateDriver.bind(this);
    this.deleteDriver = this.deleteDriver.bind(this);

    this.setTime = this.setTime.bind(this);
  }

  setEvent(event) {
    this.setState({
      event,
    });
  }

  setDrivers(drivers) {
    this.setState({
      drivers,
    });
  }

  setConfig(name, value) {
    const { event } = this.state;
    this.setState({
      event: { ...event, [name]: value },
    });
  }

  setBasicRuns(courses, runs, toCount) {
    const { event } = this.state;
    const newConfig = {};
    for (let i = 1; i <= parseInt(courses, 10); i += 1) {
      newConfig[i] = { runs: parseInt(runs, 10), toCount: parseInt(toCount, 10) };
    }

    this.setState({
      event: { ...event, config: newConfig },
    });
  }

  async setTime(eventId, id, course, run, time) {
    const { drivers, event } = this.state;
    let updatedDriver;
    this.setState({
      drivers: drivers.map((driver) => {
        if (driver.id === id) {
          updatedDriver = {
            ...driver,
            times: {
              ...driver.times,
              [course]: {
                ...driver.times[course],
                [run]: time,
                total: calculateRunTotal(
                  event.config[course].toCount,
                  { ...driver.times[course], [run]: time },
                ),
              },
            },
          };

          updatedDriver = {
            ...updatedDriver,
            times: {
              ...updatedDriver.times,
              total: calculateTotal(Object.keys(event.config).length, updatedDriver.times),
            },
          };
          return updatedDriver;
        }
        return driver;
      }),
    });
    await DriversService.saveDriver(eventId, updatedDriver);
  }

  async addDriver(eventId, driver) {
    const { drivers } = this.state;
    this.setState({
      drivers: [...drivers, driver],
    });
    await DriversService.saveDriver(eventId, driver);
  }

  async updateDriver(eventId, id, field, value) {
    const { drivers } = this.state;
    let updatedDriver;
    this.setState({
      drivers: drivers.map((driver) => {
        if (driver.id === id) {
          updatedDriver = { ...driver, [field]: value };
          return updatedDriver;
        }
        return driver;
      }),
    });
    await DriversService.saveDriver(eventId, updatedDriver);
  }

  async deleteDriver(eventId, id) {
    const { drivers } = this.state;
    this.setState({
      drivers: drivers.filter(driver => driver.id !== id),
    });
    await DriversService.deleteDriver(eventId, id);
  }

  render() {
    const {
      event,
      drivers,
    } = this.state;

    return (
      <MuiThemeProvider theme={theme}>
        <Router>
          <div className="App">
            <AutosoloHeader />
            <Route
              path="/"
              exact
              render={() => <AutosoloHomeContainer />}
            />
            <Route
              path="/:eventId/edit"
              exact
              render={() => (
                <AutosoloCreateContainer
                  event={event}
                  setEvent={this.setEvent}
                  setConfig={this.setConfig}
                  setBasicRuns={this.setBasicRuns}
                />
              )}
            />
            <Route
              path="/:eventId/drivers"
              exact
              render={() => (
                <AutosoloDriversContainer
                  drivers={drivers}
                  setDrivers={this.setDrivers}
                  addDriver={this.addDriver}
                  updateDriver={this.updateDriver}
                  deleteDriver={this.deleteDriver}
                />
              )}
            />
            <Route
              path="/:eventId/autosolo"
              exact
              render={() => (
                <AutosoloEventContainer
                  event={event}
                  drivers={drivers}
                  setEvent={this.setEvent}
                  setDrivers={this.setDrivers}
                  setTime={this.setTime}
                />
              )}
            />
            <Route
              path="/:eventId/graph"
              exact
              render={() => (
                <AutosoloGraphContainer
                  event={event}
                  drivers={drivers}
                  setEvent={this.setEvent}
                  setDrivers={this.setDrivers}
                />
              )}
            />
            <Route
              path="/:eventId/time"
              exact
              render={() => (
                <AutosoloTimekeeperContainer
                  event={event}
                  drivers={drivers}
                  setEvent={this.setEvent}
                  setDrivers={this.setDrivers}
                  setTime={this.setTime}
                />
              )}
            />
          </div>
        </Router>
      </MuiThemeProvider>
    );
  }
}

export default App;
