import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import ToolBar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import bmcLogo from './bmcLogo.gif';
import './AutosoloHeader.css';

const AutosoloHeader = () => (
  <AppBar position="relative">
    <ToolBar>
      <Link to="/">
        <img className="autosoloheader__logo" src={bmcLogo} alt="Logo" />
      </Link>
      <Typography className="autosoloheader__text" variant="h6" color="inherit" noWrap>
        Autosolo Timing
      </Typography>
    </ToolBar>
  </AppBar>
);

export default AutosoloHeader;
