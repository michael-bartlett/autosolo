const API_URL = process.env.API_URL || 'http://localhost:5000';


const config = {
  apiUrl: API_URL,
  decimalPlaces: 1,
};

module.exports = { config };
