import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { useHistory } from 'react-router-dom';

import './AutosoloCreate.css';

const AutosoloCreate = ({
  setConfig,
  setBasicRuns,
  event,
  next,
}) => {
  const history = useHistory();

  const handleChange = name => (changeEvent) => {
    setConfig(name, changeEvent.target.value);
  };

  const handleChangeRun = (changeEvent) => {
    // eslint-disable-next-line react/prop-types
    setBasicRuns(Object.keys(event.config).length, changeEvent.target.value, event.config['1'].toCount);
  };

  const handleChangeToCount = (changeEvent) => {
    // eslint-disable-next-line react/prop-types
    setBasicRuns(Object.keys(event.config).length, event.config['1'].runs, changeEvent.target.value);
  };

  const handleChangeToCourses = (changeEvent) => {
    // eslint-disable-next-line react/prop-types
    setBasicRuns(changeEvent.target.value, event.config['1'].runs, event.config['1'].toCount);
  };

  const handleChangeDate = name => (changeEvent) => {
    setConfig(name, changeEvent);
  };

  const home = () => {
    history.push('');
  };

  return (
    <Paper className="App-paper-with-buttons">
      {event.id && (
        <form>
          <TextField
            className="configInput"
            id="event-name"
            label="Name of Event"
            onChange={handleChange('name')}
            value={event.name}
          />
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              className="configInput"
              margin="normal"
              id="mui-pickers-date"
              label="Event Date"
              value={event.date}
              onChange={handleChangeDate('date')}
              KeyboardButtonProps={{
                'aria-label': 'change date',
              }}
            />
          </MuiPickersUtilsProvider>
          <TextField
            className="configInput"
            id="no-courses"
            type="number"
            label="Number of Courses"
            onChange={handleChangeToCourses}
            value={Object.keys(event.config).length}
          />
          <TextField
            className="configInput"
            id="no-runs"
            type="number"
            label="Number of runs per course"
            onChange={handleChangeRun}
            // eslint-disable-next-line react/prop-types
            value={event.config['1'].runs}
          />
          <TextField
            className="configInput"
            id="no-count"
            type="number"
            label="Number of runs per course which contribute to the final time"
            onChange={handleChangeToCount}
            // eslint-disable-next-line react/prop-types
            value={event.config['1'].toCount}
          />
        </form>
      )}
      <div className="autosolocreate__buttons">
        <Button variant="contained" onClick={home}>
          Cancel
        </Button>
        <Button variant="contained" color="primary" onClick={next}>
          Next
        </Button>
      </div>
    </Paper>
  );
};

AutosoloCreate.propTypes = {
  setConfig: PropTypes.func.isRequired,
  setBasicRuns: PropTypes.func.isRequired,
  event: PropTypes.objectOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    date: PropTypes.instanceOf(Date),
    config: PropTypes.objectOf(PropTypes.shape({
      runs: PropTypes.number,
      toCount: PropTypes.number,
    })),
  })).isRequired,
  next: PropTypes.func.isRequired,
};

export default AutosoloCreate;
