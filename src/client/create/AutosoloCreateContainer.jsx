import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import AutosoloCreate from './AutosoloCreate';
import EventsService from '../services/EventsService';

const getInitialState = eventId => ({
  id: eventId,
  name: '',
  date: new Date(),
  config: {
    1: {
      runs: 3,
      toCount: 2,
    },
    2: {
      runs: 3,
      toCount: 2,
    },
    3: {
      runs: 3,
      toCount: 2,
    },
    4: {
      runs: 3,
      toCount: 2,
    },
  },
});

class AutosoloCreateContainer extends React.Component {
  constructor(props) {
    super(props);

    this.drivers = this.drivers.bind(this);
  }

  async componentDidMount() {
    const { match: { params: { eventId } }, setEvent } = this.props;
    const event = await EventsService.getEvent(eventId);
    setEvent(event || getInitialState(eventId));
  }

  async drivers() {
    const {
      event,
    } = this.props;
    await EventsService.saveEvent(event);
    const {
      history,
    } = this.props;
    const path = 'drivers';
    history.push(path);
  }

  render() {
    const {
      setConfig,
      setBasicRuns,
      event,
    } = this.props;

    return (
      <AutosoloCreate
        setConfig={setConfig}
        setBasicRuns={setBasicRuns}
        event={event}
        next={this.drivers}
      />
    );
  }
}

AutosoloCreateContainer.propTypes = {
  setConfig: PropTypes.func.isRequired,
  setBasicRuns: PropTypes.func.isRequired,
  setEvent: PropTypes.func.isRequired,
  history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
  event: PropTypes.objectOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    date: PropTypes.instanceOf(Date),
    config: PropTypes.objectOf(PropTypes.shape({
      runs: PropTypes.number,
      toCount: PropTypes.number,
    })),
  })).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      eventId: PropTypes.node,
    }).isRequired,
  }).isRequired,
};

export default withRouter(AutosoloCreateContainer);
