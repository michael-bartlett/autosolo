import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TextField from '@material-ui/core/TextField';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import { ObjectId } from 'bson';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import { useHistory, useParams } from 'react-router-dom';

import './AutosoloDrivers.css';

const AutosoloDrivers = ({
  drivers,
  addDriver,
  updateDriver,
  deleteDriver,
}) => {
  const history = useHistory();
  const {
    eventId,
  } = useParams();

  const home = () => {
    history.push('');
  };

  const autosolo = () => {
    history.push('autosolo');
  };

  const addNewDriver = () => {
    const newId = new ObjectId();

    const newDriver = {
      id: newId.toString(),
      name: '',
      number: undefined,
      group: undefined,
      class: undefined,
      times: {},
    };
    addDriver(eventId, newDriver);
  };

  return (
    <Paper className="App-paper-with-buttons">
      <div className="App-table-overflow">
        <Table>
          <TableHead>
            <TableRow>
              <TableCell className="autosolodrivers__numbercell">Number</TableCell>
              <TableCell className="autosolodrivers__namecell">Name</TableCell>
              <TableCell className="autosolodrivers__numbercell">Class</TableCell>
              <TableCell className="autosolodrivers__numbercell">Group</TableCell>
              <TableCell className="autosolodrivers__numbercell" />
            </TableRow>
          </TableHead>
          <TableBody>
            {drivers.map(driver => (
              <TableRow key={driver.id}>
                <TableCell>
                  <TextField
                    value={driver.number || ''}
                    onChange={event => updateDriver(eventId, driver.id, 'number', event.target.value)}
                  />
                </TableCell>
                <TableCell>
                  <TextField
                    className="autosolodrivers__namecell"
                    value={driver.name || ''}
                    onChange={event => updateDriver(eventId, driver.id, 'name', event.target.value)}
                  />
                </TableCell>
                <TableCell>
                  <TextField
                    value={driver.class || ''}
                    onChange={event => updateDriver(eventId, driver.id, 'class', event.target.value)}
                  />
                </TableCell>
                <TableCell>
                  <TextField
                    value={driver.group || ''}
                    onChange={event => updateDriver(eventId, driver.id, 'group', event.target.value)}
                  />
                </TableCell>
                <TableCell>
                  <IconButton onClick={() => deleteDriver(eventId, driver.id)}>
                    <DeleteIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
      <Fab className="autosolodrivers__addbutton" color="primary" aria-label="add" onClick={addNewDriver}>
        <AddIcon />
      </Fab>
      <div className="autosolocreate__buttons">
        <Button variant="contained" onClick={home}>
          Cancel
        </Button>
        <Button variant="contained" color="primary" onClick={autosolo}>
          Next
        </Button>
      </div>
    </Paper>
  );
};

AutosoloDrivers.propTypes = {
  drivers: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    number: PropTypes.string.number,
    times: PropTypes.object,
  })).isRequired,
  addDriver: PropTypes.func.isRequired,
  updateDriver: PropTypes.func.isRequired,
  deleteDriver: PropTypes.func.isRequired,
};

export default AutosoloDrivers;
