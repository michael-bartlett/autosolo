import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import AutosoloDrivers from './AutosoloDrivers';
import DriversService from '../services/DriversService';

class AutosoloDriversContainer extends React.Component {
  async componentDidMount() {
    const {
      match: { params: { eventId } },
      setDrivers,
    } = this.props;
    const drivers = await DriversService.getDrivers(eventId);
    setDrivers(drivers);
  }

  render() {
    const {
      drivers,
      addDriver,
      updateDriver,
      deleteDriver,
    } = this.props;

    return (
      <AutosoloDrivers
        drivers={drivers}
        addDriver={addDriver}
        updateDriver={updateDriver}
        deleteDriver={deleteDriver}
      />
    );
  }
}

AutosoloDriversContainer.propTypes = {
  setDrivers: PropTypes.func.isRequired,
  drivers: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    number: PropTypes.string.number,
    times: PropTypes.object,
  })).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      eventId: PropTypes.node,
    }).isRequired,
  }).isRequired,
  addDriver: PropTypes.func.isRequired,
  updateDriver: PropTypes.func.isRequired,
  deleteDriver: PropTypes.func.isRequired,
};

export default withRouter(AutosoloDriversContainer);
