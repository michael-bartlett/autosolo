import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';

import './AutosoloEvent.css';

const applyFilters = (driver, groupFilter, classFilter) => {
  if (groupFilter && groupFilter.length > 0 && classFilter && classFilter.length > 0) {
    return (driver.group === groupFilter && driver.class === classFilter);
  }
  if (groupFilter && groupFilter.length > 0) {
    return (driver.group === groupFilter);
  }
  if (classFilter && classFilter.length > 0) {
    return (driver.class === classFilter);
  }
  return true;
};

const orderAndFilterDrivers = (drivers, groupFilter, classFilter) => {
  if (drivers.length > 0) {
    return drivers.filter(driver => applyFilters(driver, groupFilter, classFilter))
      .sort((a, b) => parseInt(a.number, 10) - parseInt(b.number, 10));
  }
  return [];
};

const AutosoloEvent = ({
  drivers,
  event,
  setTime,
}) => {
  const [groupFilter, setGroupFilter] = useState('');
  const [classFilter, setClassFilter] = useState('');
  const [tempDrivers, setTempDrivers] = useState([]);

  useEffect(() => {
    setTempDrivers(drivers);
  }, []);

  const updateTime = (driverId, course, run, time) => {
    let updatedDriver;
    setTempDrivers(tempDrivers.map((driver) => {
      if (driver.id === driverId) {
        updatedDriver = {
          ...driver,
          times: {
            ...driver.times,
            [course]: {
              ...driver.times[course],
              [run]: time,
            },
          },
        };
        return updatedDriver;
      }
      return driver;
    }));

    setTime(event.id, driverId, course, run, parseFloat(time, 10) || undefined);
  };

  const firstHeader = [];
  const secondHeader = [];
  let row;

  if (event.config) {
    for (let i = 0; i < Object.keys(event.config).length; i += 1) {
      firstHeader.push(
        <TableCell align="center" colSpan={parseInt(event.config[i + 1].runs, 10) + 1}>
          {'Course '}
          {i + 1}
        </TableCell>,
      );
      for (let j = 0; j < parseInt(event.config[i + 1].runs, 10); j += 1) {
        secondHeader.push(<TableCell className="autosoloevent__compactheader" align="right">{j + 1}</TableCell>);
      }
      secondHeader.push(<TableCell className="autosoloevent__compactheader autosoloevent__total" align="right">Total</TableCell>);
    }

    if (tempDrivers.length > 0) {
      const orderedDrivers = orderAndFilterDrivers(drivers, groupFilter, classFilter);
      row = orderAndFilterDrivers(tempDrivers, groupFilter, classFilter).map((driver, index) => {
        const cells = [];
        for (let i = 1; i <= Object.keys(event.config).length; i += 1) {
          for (let j = 1; j <= parseInt(event.config[i].runs, 10); j += 1) {
            cells.push(
              <TableCell align="right" className="autosoloevent__compactcell">
                <TextField
                  className="autosoloevent__input"
                  variant="outlined"
                  value={(driver.times[i] && driver.times[i][j]) || ''}
                  onChange={changeEvent => (
                    updateTime(driver.id, i, j, changeEvent.target.value)
                  )}
                />
              </TableCell>,
            );
          }
          cells.push(
            <TableCell align="right" className="autosoloevent__compactcell autosoloevent__total">
              {orderedDrivers[index]
                && orderedDrivers[index].times[i]
                && !!orderedDrivers[index].times[i].total
                && orderedDrivers[index].times[i].total}
            </TableCell>,
          );
        }
        cells.push(
          <TableCell classname="autosoloevent__total">
            {orderedDrivers[index]
              && orderedDrivers[index].times
              && orderedDrivers[index].times.total}
          </TableCell>,
        );

        return (
          <TableRow key={driver.id}>
            <TableCell>{driver.number}</TableCell>
            <TableCell>{driver.name}</TableCell>
            {cells}
          </TableRow>
        );
      });
    }
  }

  return (
    <Paper className="App-paper">
      <div className="autosoloevent__tableheader">
        <Typography>{event.name}</Typography>
        <div className="autosoloevent__filters">
          <FormControl className="autosoloevent__formcontrol">
            <InputLabel htmlFor="group-filter">Group</InputLabel>
            <Select
              className="autosoloevent__select"
              value={groupFilter}
              onChange={e => setGroupFilter(e.target.value)}
              inputProps={{
                name: 'group',
                id: 'group-filter',
              }}
            >
              <MenuItem value="" />
              { Array.from(new Set(
                drivers.map(driver => driver.group)
                  .filter(driverGroup => !!driverGroup)
                  .sort(),
              )).map(g => (
                <MenuItem value={g}>{g}</MenuItem>
              ))}
            </Select>
          </FormControl>
          <FormControl className="autosoloevent__formcontrol">
            <InputLabel htmlFor="class-filter">Class</InputLabel>
            <Select
              className="autosoloevent__select"
              value={classFilter}
              onChange={e => setClassFilter(e.target.value)}
              inputProps={{
                name: 'class',
                id: 'class-filter',
              }}
            >
              <MenuItem value="" />
              { Array.from(new Set(
                drivers.map(driver => driver.class)
                  .filter(driverClass => !!driverClass)
                  .sort(),
              )).map(c => (
                <MenuItem value={c}>{c}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>
      </div>
      {event.config && (
        <div className="App-table-overflow">
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Number</TableCell>
                <TableCell>Name</TableCell>
                {firstHeader}
                <TableCell>Total</TableCell>
              </TableRow>
              <TableRow>
                <TableCell />
                <TableCell />
                {secondHeader}
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {row}
            </TableBody>
          </Table>
        </div>
      )}
    </Paper>
  );
};

AutosoloEvent.propTypes = {
  event: PropTypes.objectOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    date: PropTypes.instanceOf(Date),
    config: PropTypes.objectOf(PropTypes.shape({
      runs: PropTypes.number,
      toCount: PropTypes.number,
    })),
  })).isRequired,
  drivers: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    number: PropTypes.string.number,
    times: PropTypes.object,
  })).isRequired,
  setTime: PropTypes.func.isRequired,
};

export default AutosoloEvent;
