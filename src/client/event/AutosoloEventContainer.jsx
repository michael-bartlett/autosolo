import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import AutosoloEvent from './AutosoloEvent';
import DriversService from '../services/DriversService';
import EventsService from '../services/EventsService';

class AutosoloEventContainer extends React.Component {
  async componentDidMount() {
    const {
      match: { params: { eventId } },
      setEvent,
      setDrivers,
    } = this.props;

    const event = await EventsService.getEvent(eventId);
    setEvent(event);

    const drivers = await DriversService.getDrivers(eventId);
    setDrivers(drivers);
  }

  componentDidUpdate(prevProps) {
    const { drivers } = this.props;
    return prevProps.drivers.length === drivers.length;
  }

  render() {
    const {
      event,
      drivers,
      setTime,
    } = this.props;

    return (
      <div>
        {event
          && drivers
          && drivers.length > 0
          && (<AutosoloEvent event={event} drivers={drivers} setTime={setTime} />)}
      </div>
    );
  }
}

AutosoloEventContainer.propTypes = {
  event: PropTypes.objectOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    date: PropTypes.instanceOf(Date),
    config: PropTypes.objectOf(PropTypes.shape({
      runs: PropTypes.number,
      toCount: PropTypes.number,
    })),
  })).isRequired,
  drivers: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    number: PropTypes.string.number,
    times: PropTypes.object,
  })).isRequired,
  setEvent: PropTypes.func.isRequired,
  setDrivers: PropTypes.func.isRequired,
  setTime: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      eventId: PropTypes.node,
    }).isRequired,
  }).isRequired,
};

export default withRouter(AutosoloEventContainer);
