import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { withRouter } from 'react-router-dom';

import {
  ResponsiveContainer,
  LineChart,
  Line,
  CartesianGrid,
  Tooltip,
  XAxis,
  YAxis,
  Legend,
} from 'recharts';

import './AutosoloGraph.css';

const colours = ['#e6194B', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#42d4f4', '#f032e6', '#bfef45', '#fabebe', '#469990', '#e6beff', '#9A6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#a9a9a9', '#ffffff', '#000000'];

const AutosoloGraph = ({
  event,
  drivers,
}) => {
  const [classFilter, setClassFilter] = useState('');
  const [normaliseDriver, setnormaliseDriver] = useState('');

  const setClassFilterResetDriver = (value) => {
    setClassFilter(value);
    if (normaliseDriver && normaliseDriver.class !== value) {
      setnormaliseDriver(undefined);
    }
  };

  const filteredDrivers = drivers.filter((driver) => {
    if (classFilter.length > 0) {
      return (driver.class === classFilter);
    }
    return true;
  });

  let graphData;
  if (event && event.config) {
    graphData = Object.keys(event.config).map((course) => {
      let times = {};
      filteredDrivers.forEach((driverInc) => {
        if (driverInc.times && driverInc.times[course] && driverInc.times[course].total) {
          if (normaliseDriver
                && normaliseDriver.times
                && normaliseDriver.times[course]
                && normaliseDriver.times[course].total) {
            times = {
              ...times,
              [driverInc.name]: [...Array(parseInt(course, 10)).keys()]
                .map(index => (
                  driverInc.times[index + 1].total * 100
                    - normaliseDriver.times[index + 1].total * 100
                )).reduce((a, b) => (a + b)) / 100,
            };
          } else if (!normaliseDriver) {
            times = {
              ...times,
              [driverInc.name]: [...Array(parseInt(course, 10)).keys()]
                .map(index => (driverInc.times[index + 1].total * 100))
                .reduce((a, b) => a + b) / 100,
            };
          }
        }
      });
      return {
        name: `Course ${course}`,
        ...times,
      };
    });
  }

  return (
    <Paper className="App-paper">
      <div className="autosolograph__tableheader">
        <Typography>{event.name}</Typography>
        <div className="autosolograph__filters">
          <FormControl className="autosolograph__formcontrol">
            <InputLabel htmlFor="class-filter">Class</InputLabel>
            <Select
              className="autosolograph__select"
              autoWidth
              value={classFilter}
              onChange={e => setClassFilterResetDriver(e.target.value)}
              inputProps={{
                name: 'class',
                id: 'class-filter',
              }}
            >
              <MenuItem value="" />
              { Array.from(new Set(
                drivers.map(driver => driver.class)
                  .filter(driverClass => !!driverClass)
                  .sort(),
              )).map(c => (
                <MenuItem key={c} value={c}>{c}</MenuItem>
              ))}
            </Select>
          </FormControl>
          <FormControl className="autosolograph__formcontrol">
            <InputLabel htmlFor="class-filter">Normalise Against</InputLabel>
            <Select
              className="autosolograph__namefilters"
              autoWidth
              value={normaliseDriver}
              onChange={e => setnormaliseDriver(e.target.value)}
              inputProps={{
                name: 'driver',
                id: 'driver-filter',
              }}
            >
              <MenuItem value="" />
              {
                drivers.filter(driver => (!classFilter || classFilter === driver.class))
                  .map(driver => (
                    <MenuItem value={driver}>{driver.name}</MenuItem>
                  ))
              }
            </Select>
          </FormControl>
        </div>
      </div>
      { graphData && (
        <ResponsiveContainer>
          <LineChart width={800} height={400} data={graphData}>
            <Legend />
            { filteredDrivers.map((obj, index) => (
              <Line type="monotone" dataKey={obj.name} stroke={colours[(index % colours.length)]} />
            ))}
            <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
          </LineChart>
        </ResponsiveContainer>
      )}
    </Paper>
  );
};

AutosoloGraph.propTypes = {
  event: PropTypes.objectOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    date: PropTypes.instanceOf(Date),
    config: PropTypes.objectOf(PropTypes.shape({
      runs: PropTypes.number,
      toCount: PropTypes.number,
    })),
  })).isRequired,
  drivers: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    number: PropTypes.string.number,
    times: PropTypes.object,
  })).isRequired,
};

export default withRouter(AutosoloGraph);
