import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import AutosoloGraph from './AutosoloGraph';
import DriversService from '../services/DriversService';
import EventsService from '../services/EventsService';

class AutosoloGraphContainer extends React.Component {
  async componentDidMount() {
    const {
      match: { params: { eventId } },
      setEvent,
      setDrivers,
    } = this.props;

    const event = await EventsService.getEvent(eventId);
    setEvent(event);

    const tempDrivers = await DriversService.getDrivers(eventId);
    setDrivers(tempDrivers);
  }

  render() {
    const {
      event,
      drivers,
    } = this.props;

    return (
      <AutosoloGraph event={event} drivers={drivers} />
    );
  }
}

AutosoloGraphContainer.propTypes = {
  event: PropTypes.objectOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    date: PropTypes.instanceOf(Date),
    config: PropTypes.objectOf(PropTypes.shape({
      runs: PropTypes.number,
      toCount: PropTypes.number,
    })),
  })).isRequired,
  drivers: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    number: PropTypes.string.number,
    times: PropTypes.object,
  })).isRequired,
  setEvent: PropTypes.func.isRequired,
  setDrivers: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      eventId: PropTypes.node,
    }).isRequired,
  }).isRequired,
};

export default withRouter(AutosoloGraphContainer);
