import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import TimerIcon from '@material-ui/icons/Timer';
import PreviewIcon from '@material-ui/icons/Pageview';
import DeleteIcon from '@material-ui/icons/Delete';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import EditIcon from '@material-ui/icons/Edit';
import { ObjectId } from 'bson';

import { useHistory } from 'react-router-dom';

import './AutosoloHome.css';


const AutosoloHome = ({
  events,
  deleteEvent,
}) => {
  const history = useHistory();

  const gotoEvent = (id) => {
    history.push(`${id}/autosolo`);
  };

  const gotoEventGraph = (id) => {
    history.push(`${id}/graph`);
  };

  const gotoTimekeeper = (id) => {
    history.push(`${id}/time`);
  };

  const editEvent = (id) => {
    history.push(`${id}/edit`);
  };

  const create = () => {
    const newId = new ObjectId();
    history.push(`${newId.toString()}/edit`);
  };

  return (
    <Paper className="App-paper">
      <Button variant="contained" color="primary" onClick={create}>
        Create New Autosolo
      </Button>
      {!events.length && (
        <div className="autosoloHome__existing">
          <Typography>
            No Existing Autosolos Found
          </Typography>
        </div>
      )}
      {!!events.length && (
        <div className="autosoloHome__existing">
          <Typography>
            Existing Autosolos
          </Typography>
          <div className="App-table-overflow">
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>
                    Event
                  </TableCell>
                  <TableCell>
                    Date
                  </TableCell>
                  <TableCell />
                  <TableCell />
                  <TableCell />
                  <TableCell />
                  <TableCell />
                </TableRow>
              </TableHead>
              <TableBody>
                { events.map(event => (
                  <TableRow key={event.id}>
                    <TableCell>
                      {event.name}
                    </TableCell>
                    <TableCell>
                      {event.date.toDateString()}
                    </TableCell>
                    <TableCell className="autosolohome__tablebutton">
                      <IconButton onClick={() => gotoTimekeeper(event.id)}>
                        <TimerIcon />
                      </IconButton>
                    </TableCell>
                    <TableCell className="autosolohome__tablebutton">
                      <IconButton onClick={() => editEvent(event.id)}>
                        <EditIcon />
                      </IconButton>
                    </TableCell>
                    <TableCell className="autosolohome__tablebutton">
                      <IconButton onClick={() => gotoEvent(event.id)}>
                        <PreviewIcon />
                      </IconButton>
                    </TableCell>
                    <TableCell className="autosolohome__tablebutton">
                      <IconButton onClick={() => gotoEventGraph(event.id)}>
                        <TrendingUpIcon />
                      </IconButton>
                    </TableCell>
                    <TableCell className="autosolohome__tablebutton">
                      <IconButton onClick={() => deleteEvent(event.id)}>
                        <DeleteIcon />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))
              }
              </TableBody>
            </Table>
          </div>
        </div>
      )}
    </Paper>
  );
};

AutosoloHome.propTypes = {
  events: PropTypes.arrayOf(
    PropTypes.objectOf(PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      date: PropTypes.instanceOf(Date),
      config: PropTypes.objectOf(PropTypes.shape({
        runs: PropTypes.number,
        toCount: PropTypes.number,
      })),
    })),
  ).isRequired,
  deleteEvent: PropTypes.func.isRequired,
};

export default AutosoloHome;
