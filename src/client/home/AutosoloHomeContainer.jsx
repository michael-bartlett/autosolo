import React from 'react';
import EventsService from '../services/EventsService';
import AutosoloHome from './AutosoloHome';

class AutosoloHomeContainer extends React.Component {
  constructor(props) {
    super(props);
    this.deleteEvent = this.deleteEvent.bind(this);
    this.state = {
      events: undefined,
    };
  }

  async componentDidMount() {
    const events = await EventsService.getEvents();
    this.setState({ events });
  }

  async deleteEvent(id) {
    await EventsService.deleteEvent(id);
    const events = await EventsService.getEvents();
    this.setState({ events });
  }

  render() {
    const { events } = this.state;
    return (
      <div>
        {events && (<AutosoloHome events={events} deleteEvent={this.deleteEvent} />)}
      </div>
    );
  }
}

export default AutosoloHomeContainer;
