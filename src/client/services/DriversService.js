import { config } from '../config';


async function getDrivers(eventId) {
  const response = await fetch(`${config.apiUrl}/events/${eventId}/drivers`);
  const data = await response.json();
  return data;
}

async function saveDriver(eventId, driver) {
  await fetch(`${config.apiUrl}/events/${eventId}/drivers/`,
    {
      body: JSON.stringify(driver),
      headers: { 'Content-Type': 'application/json' },
      method: 'put',
    });
  return undefined;
}

async function deleteDriver(eventId, id) {
  const response = await fetch(`${config.apiUrl}/events/${eventId}/drivers/${id}`,
    {
      method: 'delete',
    });
  return response;
}

export default {
  getDrivers,
  saveDriver,
  deleteDriver,
};
