import { config } from '../config';


async function getEvents() {
  const response = await fetch(`${config.apiUrl}/events`);
  const data = await response.json();
  return data.map(event => ({ ...event, date: new Date(event.date) }));
}

async function getEvent(id) {
  const response = await fetch(`${config.apiUrl}/events/${id}`);
  if (response.status === 200) {
    const event = await response.json();
    return { ...event, date: new Date(event.date) };
  }
  return undefined;
}

async function saveEvent(event) {
  await fetch(`${config.apiUrl}/events`,
    {
      body: JSON.stringify(event),
      headers: { 'Content-Type': 'application/json' },
      method: 'put',
    });
  return undefined;
}

async function deleteEvent(id) {
  const response = await fetch(`${config.apiUrl}/events/${id}`,
    {
      method: 'delete',
    });
  return response;
}

export default {
  getEvents,
  getEvent,
  saveEvent,
  deleteEvent,
};
