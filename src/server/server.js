const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const { MongoClient, ObjectId } = require('mongodb');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');

const swaggerDocument = YAML.load(path.resolve('./src/server/swagger.yml'));


const app = express();
const port = process.env.PORT || 5000;

const dbUrl = 'mongodb://localhost:27017';
const dbName = 'autosolo';

let db;

// app.use(express.static('build'))
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.get('/events', (request, response) => {
  db.collection('events').find().toArray((err, results) => {
    if (!err) {
      response.json(results.map((result) => {
        const { _id, ...event } = result;
        return { id: _id, ...event };
      }));
    }
  });
});

app.get('/events/:id', (request, response) => {
  db.collection('events').findOne({ _id: ObjectId(request.params.id) }, (err, result) => {
    if (!err && result) {
      const { _id, ...event } = result;
      response.json({ id: _id, ...event });
    } else if (!err && !result) {
      response.sendStatus(404);
    } else {
      response.sendStatus(500);
    }
  });
});

app.put('/events', (req, res) => {
  const { id, ...event } = req.body;
  db.collection('events').save({ _id: ObjectId(id), date: new Date(req.body.date), ...event }, (err) => {
    if (!err) {
      res.sendStatus(204);
    } else {
      res.sendStatus(500);
    }
  });
});

app.delete('/events/:id', (req, res) => {
  db.collection('events').deleteOne({ _id: ObjectId(req.params.id) }, (err) => {
    if (!err) {
      db.collection('drivers').deleteMany({ eventId: req.params.id }, (manyErr) => {
        if (!manyErr) {
          res.sendStatus(204);
        } else {
          res.sendStatus(500);
        }
      });
    } else {
      res.sendStatus(500);
    }
  });
});

app.get('/events/:eventId/drivers', (request, response) => {
  db.collection('drivers').find({ eventId: request.params.eventId }).toArray((err, results) => {
    response.json(results.map((result) => {
      const { _id, eventId, ...event } = result;
      return { id: _id, ...event };
    }));
  });
});

app.put('/events/:eventId/drivers', (req, res) => {
  const { id, ...driver } = req.body;
  db.collection('drivers').save({ _id: ObjectId(id), eventId: req.params.eventId, ...driver }, (err) => {
    if (!err) {
      res.sendStatus(204);
    } else {
      res.sendStatus(500);
    }
  });
});

app.delete('/events/:eventId/drivers/:id', (req, res) => {
  db.collection('drivers').deleteOne({ _id: ObjectId(req.params.id), eventId: req.params.eventId }, (err) => {
    if (!err) {
      res.sendStatus(204);
    } else {
      res.sendStatus(500);
    }
  });
});

const connectToDb = () => {
  MongoClient.connect(dbUrl, (err, database) => {
    if (err) {
      setTimeout(connectToDb, 5000);
      // eslint-disable-next-line no-console
      console.log(err);
    } else {
      db = database.db(dbName);
      app.listen(port, () => {
        // eslint-disable-next-line no-console
        console.log(`server is listening on ${port}`);
      });
    }
  });
};

connectToDb();
